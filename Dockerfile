FROM node:argon

RUN apt-get update &&\
  apt-get install -y git-core 

RUN cd /opt &&\
  git clone https://gitlab.com/mars-rubicon/marsdbjs.git &&\
  cd /opt/marsdbjs

WORKDIR /opt/marsdb

RUN npm install

CMD node db.py
