var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
var fs = require('fs');

var logs = [];

app.get('/', function (req, res) {
    res.send(logs);
});

app.ws('/ws', function (ws, req) {
    ws.on('message', function(msg) {
      console.log('Message', msg);
      logs.push(msg);
    });
});

app.listen(3000, function() {
    console.log('Logger running');
});
